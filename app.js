const fs = require('fs')
const pad = require('pad')
const randomString = require("random-string");

const config = JSON.parse(fs.readFileSync('config.json', 'utf8'))
let codecount = config.count
let format = config.format.split(",")
let filename = 'output.csv'


let integers = []
let indexes = []
let characters = []
let strings = []

for (let i = 0; i < format.length; i++) {
  let elem = format[i].trim()

  if (elem.indexOf("'") === 0) {
    strings.push(i)
  } else if (elem === "l" || elem === "u" || elem === "a") {
    characters.push(i)
  } else if (elem === "x") {
    integers.push(i)
  } else if (elem === "i") {
    indexes.push(i)  
  } else {
    throw new Error('Invalid character in format: ' + format[i]);
  }
}

let characterStringOptions = {
  length: characters.length,
  numeric: false,
  letters: true,
  special: false,
  exclude: config.exclude.split(",")
}

let integerStringOptions = {
  length: integers.length,
  numeric: true,
  letters: false,
  special: false,
  exclude: config.exclude.split(",")
}

console.log("Writing " + codecount + " codes...")
var csvFile = fs.createWriteStream(filename, {'flags': 'w'})

for (let i = 0; i < codecount; i++) {
  let indexString = pad(indexes.length, i.toString(), "0")
  let characterString = randomString(characterStringOptions)
  let integerString = randomString(integerStringOptions)

  let code = ""
  let characterIndex = 0
  let integerIndex = 0
  let indexesIndex = 0

  for (let j = 0; j < format.length; j++) { 
    let elem = format[j].trim()

    if (elem.indexOf("'") === 0) {
      code += elem.replace(/'/g, "")
    } else if (elem === "l") {
      code += characterString.charAt(characterIndex).toLowerCase()
      characterIndex += 1
    } else if (elem === "u") {
      code += characterString.charAt(characterIndex).toUpperCase()
      characterIndex += 1      
    } else if (elem === "a") {
      code += characterString.charAt(characterIndex)
      characterIndex += 1      
    } else if (elem === "x") {
      code += integerString.charAt(integerIndex)
      integerIndex += 1      
    } else if (elem === "i") {
      code += indexString.charAt(indexesIndex)
      indexesIndex += 1
    }  
  }
  
  csvFile.write(code + "\n")
  
}

console.log("Finished writing to " + filename)
csvFile.end()

