# README #

This README describes the use for CodeGenerator, how to install it and how to use it.


### What is this repository for? ###

* Generate random codes and write them to a file
* 1.0.0

### How do I get set up? ###

* Install node (if necesarry)
* Clone this repository
* Move into the root of the project: cd ./Codegenerator
* Install dependencies: npm install
* Configure the CodeGenerator by editing the config.json
* Create codes to a .csv file: npm start

### Configuration ###

The config.json file holds all the information needed to create random codes

#### count ####
The number of codes to be created. 
You probably wont want to go over 100000 since the process can halt your computer for a while and excel also has a limit of lines it can read.

#### check unique (not yet implemented!) ####
This checks the codes and outputs the number of doubles it has created (default behaviour?)

#### force unique (not yet implemented!) ####
This checks the codes and forces them to be unique. This can have influence on the max amount of codes defined in the count.
Padding files with fixed character of using sequential numbers can also help to create uniqueness

#### exclude ####
A String of characters that you dont want to use in your codes.
Usually the default value of "0o1ilOuv" will be ok since these characters can be unclear to read.

#### format ####
A string encapsuladed by double quotes "" that defines the layout of the code. Every character can be defined individually and should be seperated by a comma:

* **'AnyValue'**: Any value enclosed by single quotes can be used for litteral values
* **l**: The lowercase character l will output a random lowercase alphabetical value
* **u**: The lowercase character u will output a random uppercase alphabetical value
* **a**: The lowercase character a will output a random alphabetical value that can be uppercase or lowercase
* **x**: The lowercase character x will output a random numerical value
* **i**: The lowercase character i will output a sequential number (The number of occurences will define the 0 padding, which overrides an excluded 0 character)

### format examples ###

* **"l,l,i,i,i,l,i,i,l,l"** will output code a code like: jb000z78df as the 78th entry in the sequence
* **"'A',x,x,l,l,l,u,u,u,a,a,a,i"** will output code a code like: A83dyhTSXsTm4 as the 4th entry in the sequence

### Remarks or questions? ###

Ask Maurits van Mierlo